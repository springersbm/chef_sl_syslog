syslog_name = node[:sl_syslog][:syslog_flavour]
syslog_name ||= "rsyslog" if platform?("centos") && node[:platform_version].to_f >= 6.0
syslog_name ||= "rsyslog" if platform?(%w(debian ubuntu))
syslog_name ||= "syslog" if platform?("centos")
syslog_name ||= "rsyslog" if platform?("redhat")

case syslog_name
when "syslog"
  package "sysklogd"
when "rsyslog"

  package "rsyslog" do
    action :install
    notifies :start, "service[#{syslog_name}]"
  end

  path = if platform?(%w(debian ubuntu))
           '/etc/rsyslog.conf'
         else
           '/etc/sysconfig/rsyslog'
         end

  cookbook_file path do
    source "rsyslog.sysconfig"
    owner "root"
    group "root"
    mode "0644"
  end



  service syslog_name do
    action [ :stop, :disable ]
    only_if value_for_platform({
                                 %w(redhat centos fedora) => {
                                   :default => 'yum -q sysklogd'
                                 },
                                 %w(debian ubuntu) => {
                                   :default => 'dpkg -l | LANG=C egrep -q sysklogd'
                                 }
                               })
  end
end

# Todo, search for graylog server in my chef_environment.

template "/etc/#{syslog_name}.conf" do
  source "syslog.conf.erb"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, "service[#{syslog_name}]"
  variables ({
               :syslog_flavour => syslog_name,
               :graylog_server => node[:sl_syslog][:graylog_server]
             })
end

service syslog_name do
  action [ :enable ]
end
