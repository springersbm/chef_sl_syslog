name             "sl_syslog"
maintainer       "Springer Casper Team"
maintainer_email "casper.root@springer.com"
license          "All rights reserved"
description      "Installs/Configures syslog in a casper box"
#long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.7"
